using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.src.Models;

namespace tech_test_payment_api.src.Contexts
{
    public class DataBaseContext : DbContext
    {
        public DataBaseContext(DbContextOptions<DataBaseContext> options) : base(options)
        {

        }

        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Venda>(tab =>
            {
                tab.HasKey(tab => tab.Id);

            });

            builder.Entity<Produto>(tab =>
            {
                tab.HasIndex(e => e.Id);

            });

            builder.Entity<Vendedor>(tab =>
            {
                tab.HasKey(e => e.Id);


            });
        }


    }


}