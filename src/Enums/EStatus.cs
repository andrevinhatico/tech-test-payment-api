namespace tech_test_payment_api.src.Enums
{
    public enum EStatus
    {
        AguardandoPagamento,
        PagamentoAprovado,
        Cancelada,
        EnviadoParaTransportadora,
        Entregue

    }
}