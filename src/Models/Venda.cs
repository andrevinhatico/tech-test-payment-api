using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

using tech_test_payment_api.src.Enums;

namespace tech_test_payment_api.src.Models
{
    public class Venda
    {
        public Venda()
        {

        }
        public int Id { get; set; }
        public DateTime DataVenda { get; set; }
        public Vendedor Vendedor { get; set; }
        public List<Produto> Produtos { get; set; }
        public EStatus Status { get; set; }


    }


}