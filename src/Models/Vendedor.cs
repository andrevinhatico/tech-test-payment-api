using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.src.Models
{
    public class Vendedor
    {
        public Vendedor()
        {
            this.Nome = "template";
            this.Cpf = "00000000";
            this.Email = "email@email.com";
            this.Telefone = "00-00000000";
        }
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        
    }
}