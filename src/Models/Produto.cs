using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.src.Enums;

namespace tech_test_payment_api.src.Models
{
    public class Produto
    {
        public Produto()
        {
           

        }


        public int Id { get; set; }
        public String Nome { get; set; }
        public decimal Valor { get; set; }
        
    }
}