using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.src.Contexts;
using tech_test_payment_api.src.Models;

namespace tech_test_payment_api.src.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendasController : ControllerBase
    {
        [HttpGet]
        public string GetVendas()
        {
            return "GetVendas()";
        }
        [HttpGet("{id}")]
        public string GetVendasPorId(int id)
        {
            return "GetVendasPorId - " + id;
        }

        [HttpPost]
        public string AddVenda([FromBody] Venda model)
        {
            return $"Add Venda id: {model.Id} - Data: {model.DataVenda} - Vendedor: {model.Vendedor} - Produtos: {model.Produtos} - Status: {model.Status} ";
        }

        [HttpPut("[id]")]

        public string UpdateVendas(int id, [FromBody] Venda model)
        {
            return $"Update Venda id: {model.Id} - Data: {model.DataVenda} - Vendedor: {model.Vendedor} - Produtos: {model.Produtos} - Status: {model.Status} ";
        }

        [HttpDelete("{id}")]
        public string DeleteVendasPorId(int id)
        {
            return "DeleteVendasPorId - " + id;
        }







    }
}